#!/usr/bin/python3
import time
import os
import datetime as datet
from datetime import datetime, timezone
from threading import Thread
import caldav
from icalendar import Calendar, Event
import sys
import atexit

# personal.py file that contains
# url = 'URL to your CalDav Server'
# username = 'Username to your CalDav server'
# password = 'Password for your CalDav Server'
# musicpath = '?/*.mp3'
# Replace ? with the path

from personal import *

# https://github.com/thilaire/rpi-TM1638
sys.path.insert(0, '../rpi_TM1638/rpi_TM1638')
from rpi_TM1638 import TMBoards
import vlc
import _thread
from queue import Queue
import glob
import random
import alarms

# TM1638 Buttons:
# 0: Alarm toggle
# 1:
# 2: toggle bluetooth and spotify
# 3: set alarmtime
# 4:
# 5:
# 6:
# 7: set currenttime


DIO = 19
CLK = 13
STB = 26

# instanciante my TMboards
TM = TMBoards(DIO, CLK, STB, 0)

# List of all alarms that lie ahead
alarmlist = []


# toggle bluetooth and spotify service
def bt_sf_toggle():
    bluetooth_spotify = True
    TM.leds[2] = True
    while True:
        if TM.switches[2]:
            if bluetooth_spotify:
                bluetooth_spotify = False
                TM.leds[2] = False
                os.system("rfkill block bluetooth")
                os.system("systemctl stop spotifyd")
                print('Stopped Spotify and Bluetooth services')

            else:
                bluetooth_spotify = True
                TM.leds[2] = True
                os.system("rfkill unblock bluetooth")
                os.system("systemctl start spotifyd")
                print('Started Spotify and Bluetooth services')
        time.sleep(0.1)


def current_time():
    utc_dt = datetime.now(timezone.utc)  # UTC time
    current_t = utc_dt.astimezone()
    return current_t


def update_display_time():
    while True:
        try:
            TM.segments[0] = current_time().strftime("%H%M")
        except:
            print('Display Error')
        time.sleep(1.0)


def fetch_alarms():
    client = caldav.DAVClient(url=url, username=username, password=password)

    principal = client.principal()
    calendars = principal.calendars()

    for index, calendar in enumerate(calendars):
        # print(calendar.name)
        if calendar.name == 'Alarm':
            alarmcal = calendars[index]
    alarmlist = []
    for event in alarmcal.events():
        gcal = Calendar.from_ical(event.data)
        for component in gcal.walk():
            if component.name == "VEVENT":
                if component.get('summary') == 'Alarm':
                    start = component.get('dtstart')
                    if start.dt > current_time() - datet.timedelta(0, 30):
                        alarmlist.append(start.dt)
    return alarmlist


def update_alarm_list(new_list):
    alarmlist.clear()
    for new_alarm_time in new_list:
        new_alarm = alarms.Alarm(new_alarm_time, TM, musicpath)
        alarmlist.append(new_alarm)


def check_alarms():
    # Testing
    # alarmlist[0].ring()
    # time.sleep(30)

    print('Now:')
    print(current_time())
    try:
        if alarmlist[0].alarmtime - datet.timedelta(hours=24) < current_time():
            print('Next Alarm:')
            print(alarmlist[0].alarmtime)
            print('Send Alarmtime to display')
            TM.segments[4] = alarmlist[0].alarmtime.strftime("%H%M")
        else:
            print('No Alarm in the next 24h')
            for a in range(4, 8):
                for b in range(0, 8):
                    TM.segments[a, b] = False
    except:
        print('No Alarm in the next 24h')
        for a in range(4, 8):
            for b in range(0, 8):
                TM.segments[a, b] = False

    for alarm in alarmlist:
        if alarm.alarmtime < current_time():
            alarm.ring()
            del alarm
            time.sleep(30)


def exit_handler():
    TM.clearDisplay()


if __name__ == '__main__':
    TM.clearDisplay()
    atexit.register(exit_handler)

    s = Thread(target=bt_sf_toggle)
    s.start()
    print('Wireless Thread Started')

    t = Thread(target=update_display_time)
    t.start()
    print('Display Update Thread Started')

    while True:
        print('New Loop')
        try:
            print('Fetching alarms')
            al = fetch_alarms()
            try:
                print('Processing obtained alarmlist')
                update_alarm_list(al)
                check_alarms()

            except:
                print('Alarmlist could not be updated')
                print('Or failure while ringing')
        except:
            print('Fetching did not Work')
        time.sleep(5)
