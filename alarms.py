#!/usr/bin/python3
from datetime import datetime, timezone
import datetime as datet
from threading import Thread
import glob
import random
from queue import Queue
import time
import vlc


def current_time():
    utc_dt = datetime.now(timezone.utc)  # UTC time
    dt = utc_dt.astimezone()
    return dt


def blink_led(queue, tm):
    print('Blinking initialised')
    while True:
        if queue.empty():
            for nr in range(4, 8):
                tm.leds[nr] = True
            time.sleep(0.5)
            for nr in range(4, 8):
                tm.leds[nr] = False
            time.sleep(0.5)


def play_song(song_list, queue):
    playback = True
    while playback:
        for song in song_list:
            if playback:
                print(song)
                player = vlc.MediaPlayer(song)
                player.play()

                time.sleep(1.5)

                while True:
                    if queue.empty():
                        if player.get_state() == vlc.State.Playing:
                            time.sleep(0.1)
                        else:
                            player.stop()
                            print('Song finished')
                            break
                    else:
                        playback = False
                        break

                player.stop()
            else:
                break
            print('Playing next Song')

        print('Songlist finished, starting fresh')
    print('Playback Stopped')


def stop_song(queue, tm):
    while True:
        if tm.switches[0]:
            print('end playback')
            queue.put(False)
            break
        time.sleep(0.01)


class Alarm:

    def __init__(self, alarmtime, tm, musicpath):
        self.alarmtime = alarmtime
        self.tm = tm
        self.musicpath = musicpath
        print('Contructor: Found new Alarm')

    def __del__(self):
        print('Destructor: Alarm deleted')

    def ring(self):
        print(self.alarmtime)
        print(current_time())
        print('ringing')
        songlist = glob.glob(self.musicpath)
        random.shuffle(songlist)

        queue = Queue()
        stop_thread = Thread(target=stop_song, args=(queue, self.tm,))
        stop_thread.start()
        blink_thread = Thread(target=blink_led, args=(queue, self.tm,))
        blink_thread.start()

        play_song(songlist, queue)
